# appsody_ide_demo

Getting Started (Windows)
=========================
1. copy appsody.exe and dkubectl.exe from windows folder to c:\bin

2. Install Docker and git for windows.

3. open command promot and make sure git and docker command works from command prompt. During following commands, a pop-up will come to share your project drive for docker to access it. Make sure your project drive sharing is enabled in Docker settings.

4. create project folder
    mkdir dkube-pycharm-demo

5.  initialize project

appsody list 
appsody init dkube-datascience-pycharm-win

6.  Launch PyCharm and open the project

7.  Choose "Appsody Run" from configuration dropdown to run project in local container

8.  To debug the project, choose "Debug" and click on run (play icon). Edit app.py and replace localhost with your host IP. Set a breakpoint. Choose "Appsody Debug" from configuration and click run to debug the project.

9. To deploy the project on DKube, edit .dkube_job.ini in your project and fill all 4 mandatory parameters. Login to your docker registery from terminal. Choose "Appsody deploy" to run the project under DKube.


Getting Started (Mac/Ubuntu)
===========================
1. copy appsody binary from mac/ubuntu folder to /usr/local/bin. For mac, copy dkubectl binary from mac/ubuntu to ~/.dkube/cli.

cp appsody /usr/local/bin
cp dkubectl ~/.dkube/cli/

2. create project folder

mkdir demoproj

3. initialize project

appsody list 
appsody init dkube-datascience-vscode

4. Launch VSCode

    code .

5. Run the example app.py

    open command palette (ctrl+shift+p), choose Tasks: Run Task and click on Appsody: Run

6. Debugging

    open command palette (ctrl+shift+p), choose Tasks: Run Task and click on Appsody: Stop
    open command palette (ctrl+shift+p), choose Tasks: Run Task and click on Appsody: Debug
    Click on debugger and click on start debugging

7. Deploy

    edit .dkube_job.ini file and fill the mandatory fields. 
    login to docker registery from terminal by executing docker login
    open command palette (ctrl+shift+p), choose Tasks: Run Task and click on Appsody: Stop
    open command palette (ctrl+shift+p), choose Tasks: Run Task and click on Appsody: Deploy    

    
PYCHARM DEMO on Windows
======================
![Appsody PyCharm Demo](win-pycharm-demo.gif)

VSCODE DEMO
============
![Appsody VSCode Demo](demo1.gif)

PYCHARM DEMO
============
![Appsody PyCharm Demo](pycharm-demo.gif)

RSTUDIO DEMO
============
![Appsody RStudio Demo](rstudio-demo.gif)